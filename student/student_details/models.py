from __future__ import unicode_literals
from student_details.managers import StudentHandler
from django.db import models
class Course(models.Model):
	course_name = models.CharField(max_length = 40, null = True, blank = True)
	marks = models.IntegerField(null = True, blank = True)
	remarks = models.CharField(max_length = 50, default = 'Average', null = True, blank = True)
	objects = StudentHandler()
	def __unicode__(self):
		return str(self.course_name) + ' ' + str(self.marks) + ' ' + str(self.remarks) + '\n'	
class Student_Details(models.Model):
	student_id = models.IntegerField(primary_key = True)
	course_name = models.ManyToManyField(Course)
	first_name = models.CharField(max_length=200, null = True, blank = True)
	middle_name = models.CharField(max_length = 100, null = True, blank = True)
	last_name = models.CharField(max_length = 200, null = True, blank = True)
	def __unicode__(self):
		return str(self.student_id) + ' ' + str(self.student_id) + '\n'
	