from django.conf.urls import url
from student_details.views import student_view
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
  url(r'^v1/get_person/$',csrf_exempt(student_view.as_view())),
]