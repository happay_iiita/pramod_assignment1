from django.http import HttpResponse, QueryDict
from student_details.models import Student_Details, Course
from django.views.generic import View
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist

import json

class student_view(View):	
	def get(self,request,*args,**kwargs):
		_params = request.GET
		data = []
		student = Student_Details.objects.all()
				for i in student:
			data.append({
				'student_id':serializers.serialize('json', i.course_name.all().filter(course_name = 'arts')),
				'first_name':i.first_name,
				'middle_name':i.middle_name,
				'last_name':i.last_name,
			})		
		return HttpResponse(json.dumps(data), content_type = 'application/json')
	def post(self, request, *args, **kwargs):
		data = request.POST
		from .utils import grading
		course = Course(course_name = data.get('cn'), marks = data.get('marks'))
		course.save()
		course.remarks = Course.objects.grading(data.get('marks'))	
		student = Student_Details(student_id = data.get('id'), first_name=data.get('fn'), middle_name=data.get('mn'), last_name = data.get('ln'))
		student.save()
		student.course_name.add(course)
		return HttpResponse("name added succesfully") 
	def put(self, request, *args, **kwargs):
		data = QueryDict(request.body)
		try:
			temp = Student_Details.objects.get(student_id = data.get('id'))
		except ObjectDoesNotExist:
			raise send("entry doesn't exist")
		course = Course(course_name = data.get('cn'), marks = data.get('marks'))
		course.save()
		course.remarks = Course.objects.grading(data.get('marks'))		
		temp.course_name.add(course)
		temp.save()
		return HttpResponse("Courses updated")
