What This Repository is For?

This repository is for a student project where we are keeping track of the performance of 
a student in his/her respective course.
This is a django based framework.


What things are implemented?

Various method has been implemented like GET, PUT, POST to fetch, update and add the data respectively.
All the operations like migration,type conversion have all been shown inside the code itself.


How It Works?
Student_details app contain files models, view, urls all in python 
where model define the database structure we will be using for the app
views are the functionality that will happen when hitting the url and urls is for
connecting the urls of the app with the project.


HOW TO SETUP?
Run a server to use the project,and then send the http request GET, PUT, POST to get a http response.